import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from numpy.random import rand

def derivada_tanh(x):
    # x = np.tanh(x)
    x = x**2
    result = 1.0 - x
    return result

def derivada_erro(saida, label):
    return 2*saida - 2*label

def treinamento(Dados, Rotulos, n_epoch=25, learn_rate=0.2):
    w = np.array([rand()*2 - 1, rand()*2 -1 ])
    b = rand() * 2 - 1

    for i in range(n_epoch):
        for dado, rotulo in zip(Dados, Rotulos):
            p = np.sum(w * dado.T) + b
            p = np.tanh(p)
            for n in range(2):
                w[n] = w[n] - ( learn_rate/len(Dados) * derivada_erro(p, rotulo) * derivada_tanh(p) * dado[n])
            b = b - ( (learn_rate/len(Dados)) * derivada_erro(p, rotulo) * derivada_tanh(p) )
        plot_result(data, w, b)
    return w, b


def predict(x, w, b):
    p = sum(x.T * w) + b
    if np.tanh(p) > 0:
        return 1
    else:
        return -1
    # return np.tanh(p)

def plot_result(dataset, w, b):

    # Print dos pontos
    class_a = dataset[dataset[2] ==  1]
    class_b = dataset[dataset[2] == -1]
    plt.xlim(-5, 5)
    plt.ylim(-5, 5)

    # fig = plt.figure()
    # ax = plt.add_axes([0, 0, 1, 1])
    plt.scatter(class_a[0], class_a[1], color='r', s=10)
    plt.scatter(class_b[0], class_b[1], color='b', s=10)



    # Print da reta
    x = np.linspace(-5, 5, 100)

    slope = -(b/w[1])/(b/w[0])  
    intercept = -b/w[1]

    y = (x * slope) + intercept

    plt.plot(x, y, 'g')

    plt.show()


data = pd.read_csv("data.csv", header=None)

coords = np.stack((data[0], data[1]), 1 )
y = data[2]
   

w, b = treinamento(coords, y)
print('Pesos: ', w, b, '\n\n')

# Calcular Matriz confusão

true_positive = 0
true_negative = 0
false_positive = 0
false_negative = 0

for x, label in zip(coords, y):
    ans = predict(x, w, b)
    if label == 1:
        if ans == 1:
            true_positive += 1
        else:
            false_positive += 1
    else:
        if ans == -1:
           true_negative += 1
        else:
            false_negative += 1


table = pd.DataFrame([[true_negative, false_positive], [false_negative, true_positive]],
                     index=['Label Class B', 'Label Class A'], 
                     columns=['Predicted Class B', 'Predicted Class A'])

print(table)
print()
precision = true_positive/(true_positive + false_positive)
reccal = true_positive/(true_positive + false_negative)
print('Precisão: ', precision)
print('Revocação: ', reccal)

# predictions = np.array([predict(x, w, b) for x in coords])

# result = np.array([predictions == y])
# print(result)
# j = 0
# for i in result.T:
#     if i == False:
#         j += 1

plot_result(data, w, b)


